# HOT_Exercises
Hands on exercises for the 2021 Ansible Hot Session

In order to run and configure the Ansbile CI/CD pipelines navigate to the target branch of the Excercise.

## Running Ansible & GitLab HOT Session 2021 
In order to run the Ansible pipelines go to the branch marked with the exercise name.

 - Exercise 0 - Configuring GitLab: `0_configure_env`
 - Exercise 1 - Running Your First playbook: `1_run_playbook`
 - Exercise 2 - Using Variables: `2_variables`
 - Exercise 3 - Adding additional & configuring roles & pipelines: `3_additional_roles`
 - Exercise 4 - Final: Mock Applicaiton: `4_final`

![Create Project](0_configure_env/images/branch_overview.png)



Project Overview: 

![Create Project](0_configure_env/images/pipeline_overview.png)

Project Directories:
![Create Project](0_configure_env/images/dirs.png)


