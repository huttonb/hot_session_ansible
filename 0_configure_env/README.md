# Exercise 0 - Configuring the Environments 
---
##### For the first exercise we are going to configure the Gitlab Environement. Then run a `ping` module to help you get a feel for how Ansible works.  `Ping` is a test module, which always returns `pong` on successful contact. It is useful to verify the ability to login and that a usable python is configured.
---

## Step 0.0 - Import Projects

### Create Project & Import - Hot_Exercises Project
1. Menu --> Projects --> Create new project
![Create Project](images/import_project.png)
2. Import Project --> GitLab Export

![Import Project](images/gitlab_export.png)

3. Project Name --> Use Name of export: Example --> Hot_Exercises
4. Project URL --> `User Account`
5. Choose File --> hot_exercises_export.tar.gz
6. Click Import Project

![Import Project Final](images/import_project_2.png)
---


## Step 0.1 -  Register the GitLab Runner
GitLab UI
1. Select Menu on top left 
2. In the dropdown select Project --> Example: Hot_Exercises
3. Settings --> CI/CD
4. Expand Runners
5. Copy the registration token


![Runner Token](images/project_runner.png)

**NOTE**
####
> If you see `User validation required` do not worry we are not going to be using gitlab's prepopulated shared runners. 
---

### SSH to Runner VM: 
ssh into the Runner VM 
```
ssh dtu_training@<IP>
```
Edit the build script to include the registration token
```
vim /home/dtu_training runner_build-env.sh
```
Update the REGISTRATION_TOKEN variable [line 15] with the token you copied in the last step of 0.1

Before edit:
``` 
#export REGISTRATION_TOKEN=    
```

After Edit:
```
export REGISTRATION_TOKEN=<token>
```

Exit Vim (Ctrl-C)
   ```
    :wq
   ``` 
Run the shell script
```
sudo sh runner_build-env.sh
```

Validate Runner:
Project --> Setting --> CI/CD --> Expand Runners

![Runner Token](images/deployed_runner.png)
---

## Step 0.2 -  Configure GitLab CI/CD Variables
Dynatrace Tenant 
1. Create PaaS Token
2. Create API Token
    - API v2 scopes
        - Read Settings 
        - Write Settings
        - Read Metrics
        - Write Metrics
        - Read Events
        - Read Entities
        - Write Entites
    - API v1 
        - Read Configurations
        - Write Configurations 
---

## GitLab UI
1. Select Hot_Exercises project
2. Settings --> CI/CD
3. Expand Variables - Add the following Variables *Must match text & casing* - Do not check the protect box for the variables
  - PaaS Token
    - Key: paas_token
    - Value: paas token created in step 1
  - API Token
    - Key: api_token
    - Value: api_token create in step 2
  - Tenant URL - *Note: Do not have trailing* `/`
    - Key: url
    - Value: https://<id>.dev.dynatracelabs.com
  - User to authenticate into VM's
    - Key: user
    - Value: dtu_training
  - Password for dtu_training
    - Key: pass
    - Value: Grab PW from DTU


## Step 0.3 - Update Inventory File in 0_Configure_env Branch
1. Select Branch 0_Configure_env Branch from Branch Dropdown
2. Select Web IDE
Update the inventory.yml to include the IP address for the Runner & Application VM's then commit the change into the exisiting branch.

Inventory.yml --> Web Edit
``` yaml
Before:
      application:
        hosts: 
          x.x.x.x:
      runner:
        hosts:
          x.x.x.x:
After
      application:
        hosts: 
          <Application IP>:
      runner:
        hosts:
          <Runner IP>:
```

3. Commit the changes to 0_configure_env

## Step 0.4 - View CICD Pipeline
1. Left Hamburger Menu --> CI/CD
2. Pipeline --> Select Latest Pipeline

![CI/CD Pipeline](images/pipeline.png)

Succesful Pipeline 

![CI/CD Pipeline](images/ping.png)

## Step 0.5 - Completed
You have completed:
  - Configure GitLab & Ansible
  - Triggered & Viewed first CI/CD Pipeline


Continue on to `branch` *1_run_playbook* 
